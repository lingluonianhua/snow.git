#ifndef WIDGETMOVE_H
#define WIDGETMOVE_H

#include <QWidget>
#include <QMouseEvent>
#include <QPoint>

//T 为基类，继承拥有一个QWidget *parent参数的构造函数
template <typename T>
class WidgetMove : public T
{
public:
    WidgetMove(QWidget *parent = 0):T(parent)
    {
    }
protected:
    void mousePressEvent(QMouseEvent *event)
    {
        //当鼠标左键按下时，记录当前位置
        if(event->button() == Qt::LeftButton)
        {
            m_CurrentPos = event->globalPos() - T::frameGeometry().topLeft();
            event->accept();
        }
        T::mousePressEvent(event);
    }

    void mouseMoveEvent(QMouseEvent *event)
    {
        //支持窗体移动
        if (event->buttons() & Qt::LeftButton)
        {
            T::move(event->globalPos() - m_CurrentPos);
            event->accept();
        }
        T::mouseMoveEvent(event);
    }
protected:
    QPoint m_CurrentPos;
};

#endif // WIDGETMOVE_H

