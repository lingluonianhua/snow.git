#ifndef GROUNDLIFE_H
#define GROUNDLIFE_H


#include <QWidget>
#include <list>
#include <QToolButton>
#include <tchar.h>
#include <pubbase.h>

#define FAILED_SET_WALLPAPER -1

class Vegetable;
class QRect;
class QSize;

class GroundLife : public QWidget
{
	Q_OBJECT

public:
	GroundLife(QWidget *parent);
	~GroundLife();
	void InitLife();
private:
	Vegetable* CreateVegetable(const char* pngFileName, const QRect& rect, const QSize& size  = QSize());
signals:
	void Message(int errCode);
public slots:
	//处理雪花着地
	void AddTree(QWidget* parent);
	void SetBackGround(const char* imgFileName);
private:
	char m_chPath[256];
	std::list< QToolButton* > m_btnList;
};

#endif // GROUNDLIFE_H
