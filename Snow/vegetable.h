#ifndef VEGETABLE_H
#define VEGETABLE_H

#include <QToolButton>
#include "Virtual3DMove.h"
#include "PngButton.h"

//间接继承自QToolButton，支持图片按钮、支持move操作
class Vegetable : public PngButton< Virtual3DMove<QToolButton> >
{
	Q_OBJECT

public:
	Vegetable(const char* pngFileName, QWidget *parent );
	~Vegetable();

private:
	
};

#endif // VEGETABLE_H
