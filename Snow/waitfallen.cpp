#include "pubbase.h"
#include "waitfallen.h"
#include <algorithm>
#include <snownode.h>
#include <QRect>
#include <QDebug>
WaitFallen::WaitFallen(int snowPool, QWidget *parent) :
    QWidget(parent),m_snowPool(snowPool)
{
    m_waitFallen.clear();
    for(int i = 0; i < m_snowPool; i++)
    {
        SnowNode *node = new SnowNode(parent);
        Q_ASSERT(node);
        m_waitFallen.push_back(node);
    }
}


//发送雪花投入信号
void WaitFallen::SnowFalling()
{
    SNOWLIST fallingList;
    int cnt = qrand() % 3;
    static int i = 0;
    qDebug() << "num" << i++;

    for(int i = 0; i < cnt; i++)
    {
        SNOWLIST::iterator item = m_waitFallen.begin();
        if(item == m_waitFallen.end())
        {
            break;
        }
        (*item)->InitSnow();
        (*item)->setHidden(false);
        fallingList.push_back(*item);
        m_waitFallen.erase(item);
    }

    if(fallingList.size() > 0)
    {
        emit falling(&fallingList);
    }

}

//回收雪花
void WaitFallen::SnowRecycle(SNOWLIST* addList)
{
    Q_ASSERT(addList);

    for(SNOWLIST::iterator item = addList->begin(); item != addList->end(); item++)
    {
        m_waitFallen.push_back(*item);
    }

}
