#ifndef SNOWFRAME_H
#define SNOWFRAME_H

#include <QWidget>


#include "snowfalling.h"
#include "snowpack.h"
#include "waitfallen.h"

class QTimer;
class SnowFrame : public QWidget
{
    Q_OBJECT
public:
    explicit SnowFrame(QWidget *parent = 0);
    void Run();
    void Stop();
signals:

public slots:

private:
    WaitFallen m_snowWait;
    SnowFalling m_snowFalling;
    Snowpack m_snowLander;
    QTimer *m_timeManager;
    QTimer *m_timeFalling;
    QTimer *m_timeRecycle;
};

#endif // SNOWFRAME_H
