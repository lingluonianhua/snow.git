#ifndef __PNGBUTTON_H__
#define __PNGBUTTON_H__

/*********************************************************************
功能： 实现一个将png作为图形的异形按钮
	1、构造函数必须传入png图片对象
	2、SetMourseEntry函数用于设置当鼠标移动到按钮上时的大小和图片
**********************************************************************/
#include "pubbase.h"
#include <QWidget>
#include <QString>
#include <QPixmap>
#include <QBitmap>
#include <QSize>
#include <QIcon>
#include <QRect>
#include <QPoint>

class QResizeEvent;
class QPaintEvent;
class QEvent;

//T 为基类，继续拥有一个QWidget *parent参数的构造函数
template <typename T>
class PngButton : public T
{
public:
	PngButton(const char* pngFileName, QWidget *parent = 0 ):T(parent)
	{
		 setWindowFlags( Qt::FramelessWindowHint );
		 m_curImg.load(pngFileName);
		 m_normal = m_curImg.size();

		 T::resize(m_curImg.size());
	}
	//设置鼠标移动到按钮上时的大小和图片
	void SetMourseEntry(const QSize& sz , const char* pngFileName = NULL)
	{
		if (sz != QSize())
		{
			m_mourseEnter = sz;
		}
		if (pngFileName)
		{
			m_backImg.load(pngFileName);
		}
	}
	
protected:

	//鼠标进入
	void enterEvent(QEvent * event)
	{
		QRect rect = T::geometry();
		if (!m_backImg.isNull())
		{
			QPixmap tmp = m_curImg;
			m_curImg = m_backImg;
			m_backImg = tmp;

		}

		if (m_mourseEnter != QSize())
		{
			QSize tmp = m_normal;
			m_normal = m_mourseEnter;
			m_mourseEnter = tmp;
			//微调图片开始位置，优化图大缩放效果
			QPoint pos(rect.x() + (m_mourseEnter.width() - m_normal.width())/2, rect.y() + (m_mourseEnter.height() - m_normal.height())/2);
			rect = QRect(pos, m_normal);
		}
		T::enterEvent(event);
		T::setGeometry(rect);
		
		
	}
	//鼠标离开事件
	void leaveEvent(QEvent * event)
	{
		QRect rect = T::geometry();
		if (!m_backImg.isNull())
		{
			QPixmap tmp = m_curImg;
			m_curImg = m_backImg;
			m_backImg = tmp;
		}
		if (m_mourseEnter != QSize())
		{
			QSize tmp = m_normal;
			m_normal = m_mourseEnter;
			m_mourseEnter = tmp;
			//微调图片开始位置，优化图大缩放效果
			QPoint pos(rect.x() + (m_mourseEnter.width() - m_normal.width())/2, rect.y() + (m_mourseEnter.height() - m_normal.height())/2);
			rect = QRect(pos, m_normal);
		}

			
		T::leaveEvent(event);
		T::setGeometry(rect);
	}
	//大小改变时
	void resizeEvent(QResizeEvent * event) 
	{
		m_normal = T::size();
		QPixmap tmpPix = m_curImg.scaled(m_normal,Qt::IgnoreAspectRatio);
		T::setIcon(QIcon(tmpPix));
		T::setIconSize(m_normal);
		T::setMask(tmpPix.mask());
		T::resizeEvent(event);
	}

private:
	QPixmap m_curImg;
	QPixmap m_backImg;
	QSize m_normal;
	QSize m_mourseEnter;
};

#endif //__PNGBUTTON_H__