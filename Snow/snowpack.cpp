#include "pubbase.h"
#include "snowpack.h"


Snowpack::Snowpack(QWidget *parent) :
    QWidget(parent)
{
}


void Snowpack::LanderSnow(SNOWLIST* addList)
{
    for(SNOWLIST::iterator item = addList->begin(); item != addList->end(); item++)
    {
        m_snowLander.push_back(*item);
    }
    Recycle();
}

bool IsCanRecycle(SnowNode* node)
{
    return true;
}

void Snowpack::Recycle()
{
    SNOWLIST recycleList;
	//����
    SNOWLIST::iterator item = m_snowLander.begin();
    while(item != m_snowLander.end())
    {
        if(!IsCanRecycle(*item))
        {
            item++;
            continue;
        }
        SNOWLIST::iterator tmp = item;
        item++;
		recycleList.push_back(*tmp);
        m_snowLander.erase(tmp);
       
    }
    if(recycleList.size() > 0)
    {
        emit snowRecycle(&recycleList);
    }

}
