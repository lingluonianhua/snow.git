#ifndef WAITFALLEN_H
#define WAITFALLEN_H

#include <QObject>
#include <list>
#include <QTimer>
#include "snownode.h"
#include <QMutex>

class WaitFallen : public QWidget
{
    Q_OBJECT
    typedef std::list<SnowNode*> SNOWLIST;
public:
    explicit WaitFallen(int snowPool, QWidget *parent );

signals:
    //雪花飘落通知
    void falling(SNOWLIST* landerList);
public slots:
    //雪花回收处理
    void SnowRecycle(SNOWLIST* addList);
    void SnowFalling();
private:
    int m_snowPool;
    SNOWLIST m_waitFallen;

};

#endif // WAITFALLEN_H
