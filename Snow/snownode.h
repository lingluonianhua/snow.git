#ifndef SNOWNODE_H
#define SNOWNODE_H

#include <QToolButton>
#include <QPropertyAnimation>
class QPixmap;
class QPaintEvent;
class QPoint;
class QSize;

class SnowNode : public QToolButton
{
public:
    static const int MAXSWOW ;
    SnowNode(QWidget *parent = 0);
    QSize GetSnowSize();
    bool IsLander();
    void InitSnow();
    void FallingAnimation();

protected:
    void paintEvent(QPaintEvent *event);
private:
    QString GetImgFileName();
private:
    QPixmap m_pixmap;
    QPropertyAnimation *m_animation;
    QSize m_areaSize;
};

#endif // SNOWNODE_H
