#ifndef SNOWPACK_H
#define SNOWPACK_H

#include <QWidget>
#include <list>
#include <snownode.h>

class Snowpack : public QWidget
{
    Q_OBJECT
    typedef std::list<SnowNode*> SNOWLIST;
public:
    explicit Snowpack(QWidget *parent = 0);

signals:
    //发送雪花回收信号
    void snowRecycle(SNOWLIST* landerList);

public slots:
    //处理雪花着地
    void LanderSnow(SNOWLIST* addList);
    void Recycle();
private:
    SNOWLIST m_snowLander;

};

#endif // SNOWPACK_H
