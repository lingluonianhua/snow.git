#include "pubbase.h"
#include "snowfalling.h"
#include "snownode.h"
#include <QTimer>


SnowFalling::SnowFalling(QWidget *parent) :
    QWidget(parent)
{
}

void SnowFalling::AcceptSnow(SNOWLIST* addList)
{
    DEBUGTEST;
    for(SNOWLIST::iterator item = addList->begin(); item != addList->end(); item++)
    {
        Q_ASSERT(*item);
        m_snowFalling.push_back(*item);
        (*item)->FallingAnimation();
    }
    Falling();
}

void SnowFalling::Falling()
{
    DEBUGTEST;
    SnowLanding();
}

bool SnowFalling::IsLander(SnowNode* node)
{
    DEBUGTEST;
    return node->IsLander();
}

void SnowFalling::SnowLanding()
{
    DEBUGTEST;

    SNOWLIST landerList;
    SNOWLIST::iterator item = m_snowFalling.begin();
    while(item != m_snowFalling.end())
    {
        if(!IsLander(*item))
        {
            item++;
            continue;
        }
        SNOWLIST::iterator tmp = item;
        item++; 
        (*tmp)->setHidden(true);
        landerList.push_back(*tmp);
        m_snowFalling.erase(tmp);
    }

    if(landerList.size() > 0)
    {
        emit snowLander(&landerList);
    }

}


