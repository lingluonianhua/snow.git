#include "groundlife.h"
#include <windows.h>
#include "vegetable.h"
#include <QDesktopWidget>
#include <QApplication>
#include <QRect>
#include <QSize>


GroundLife::GroundLife(QWidget *parent)
	: QWidget(parent)
{
	//保存桌面背景
	::SystemParametersInfoA(SPI_GETDESKWALLPAPER, MAX_PATH,m_chPath,0);
	SetBackGround("./image/back1.jpg");
	//地面面积占屏幕的下半部分
	QSize sz(QApplication::desktop()->width(), QApplication::desktop()->height());
	this->move(0, sz.height()/2);
	this->resize(sz.width(), sz.height()/2);
	InitLife();
	
}

void GroundLife::InitLife()
{
	m_btnList.push_back(CreateVegetable("./image/lea.png", QRect(800, 200, 96, 96)));
 	m_btnList.push_back(CreateVegetable("./image/lea.png", QRect(900, 200, 96, 96)));
 	m_btnList.push_back(CreateVegetable("./image/wintersweet1.png", QRect(1200, 100, 192, 192)));
 	m_btnList.push_back(CreateVegetable("./image/wintersweet2.png", QRect(600, 0, 320, 320)));
 	m_btnList.push_back(CreateVegetable("./image/tree.png", QRect(300, 0, 256, 256)));
}

Vegetable* GroundLife::CreateVegetable(const char* pngFileName, const QRect& rect, const QSize& size)
{
	Vegetable *leaPtr = new Vegetable(pngFileName, this);
	leaPtr->resize(rect.size());
	if (size != QSize())
	{
		leaPtr->SetMourseEntry(size);
	}
	
	leaPtr->move(rect.x(), rect.y());
	return leaPtr;
}
GroundLife::~GroundLife()
{
	if (strlen(m_chPath) != 0)
	{
		SetBackGround(m_chPath);
	}
	
}

void GroundLife::SetBackGround(const char* imgFileName)
{
	 	if(!::SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, (void*)imgFileName, SPIF_UPDATEINIFILE)) //设置桌面背景  
	 	{  
	 		emit Message(FAILED_SET_WALLPAPER);  
	 	}  
}

void GroundLife::AddTree(QWidget* parent)
{


}