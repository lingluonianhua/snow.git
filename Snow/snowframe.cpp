#include "pubbase.h"
#include "snowframe.h"
#include <QTime>

SnowFrame::SnowFrame(QWidget *parent) :
    QWidget(parent),m_snowWait(100,parent),m_snowFalling(parent),m_snowLander(parent)
{
    connect(&m_snowWait, SIGNAL(falling(SNOWLIST*)), &m_snowFalling, SLOT(AcceptSnow(SNOWLIST*)));
    connect(&m_snowFalling, SIGNAL(snowLander(SNOWLIST*)), &m_snowLander, SLOT(LanderSnow(SNOWLIST*)));
    connect(&m_snowLander, SIGNAL(snowRecycle(SNOWLIST*)), &m_snowWait, SLOT(SnowRecycle(SNOWLIST*)));
    Run();
}

void SnowFrame::Run()
{
    m_timeManager = new QTimer(this);
    connect( m_timeManager,SIGNAL(timeout()), &m_snowWait, SLOT(SnowFalling()) );
    m_timeManager->start(500);

    //m_timeFalling = new QTimer(this);
    //connect( m_timeFalling,SIGNAL(timeout()), &m_snowFalling, SLOT(Falling()) );
    //m_timeFalling->start(1000);

    //m_timeRecycle = new QTimer(this);
    //connect( m_timeRecycle,SIGNAL(timeout()), &m_snowLander, SLOT(Recycle()) );
    //m_timeRecycle->start(100);
}
void SnowFrame::Stop()
{
    if ( m_timeManager->isActive() )
    {
        m_timeManager->stop();
    }
    if( m_timeFalling->isActive() )
    {
        m_timeFalling->stop();
    }
}
