#ifndef SNOWFALLING_H
#define SNOWFALLING_H

#include <QWidget>
#include <list>
#include "snownode.h"
#include <QMutex>
class SnowFalling : public QWidget
{
    Q_OBJECT
    typedef std::list<SnowNode*> SNOWLIST;
public:
    explicit SnowFalling(QWidget *parent = 0);
    void SnowLanding();
    bool IsLander(SnowNode* node);
signals:
    //雪花已着陆通知
    void snowLander(SNOWLIST* landerList);
public slots:
    //有雪花加入，接收函数
    void AcceptSnow(SNOWLIST* addList);
    void Falling();
private:
    SNOWLIST m_snowFalling;

};

#endif // SNOWFALLING_H
