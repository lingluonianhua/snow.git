#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <QToolButton>
#include <snowframe.h>
#include <QCloseEvent> 
#include "groundlife.h"

class QString;
class QSize;
class QCloseEvent;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    void InitEnv();
protected:
    void keyPressEvent( QKeyEvent * event );
	void closeEvent(QCloseEvent *event);
private:
    SnowFrame m_snowFrame;
	GroundLife *m_groundLife;
};

#endif // MAINWINDOW_H
