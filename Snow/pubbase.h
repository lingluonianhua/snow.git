#ifndef PUBBASE_H
#define PUBBASE_H



#include <QDebug>
class DebugTrace
{
public:
    DebugTrace(const char* msg)
    {
        m_msg = msg;
        qDebug() << "DebugTrace" << msg ;
    }
    ~DebugTrace()
    {
        qDebug() << "~" << m_msg ;
    }
    const char* m_msg;
};

#define DEBUGTEST DebugTrace __debug(__FUNCTION__)

#endif // PUBBASE_H
