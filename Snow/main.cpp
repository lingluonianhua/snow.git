
#include <QtPlugin>
Q_IMPORT_PLUGIN (QWindowsIntegrationPlugin);

#include <QApplication>
#include "mainwindow.h"
#include <exception>

/***************************************************
  实现方案：
    1、SnowNode雪花节点类，定义雪花的形状、初始化状态、行为
    2、WaitFallen待飘落的雪花类，用于定时向屏幕投入雪花
    3、Snowfalling 正在飘落的雪花，定时扫描雪花是否到达地上，雪花到达地上时，发送信号
    4、Snowpack 地上的积雪，定时将积雪回收
    5、GroundLife 地面物体
		Vegetable 植物
    6、SnowFrame雪花画面，对雪花进行统一管理

功能插件：
	PngButton      图片按钮
	WidgetMove     普通的按钮移动
	Virtual3DMove  将按钮虚拟成三维效果，按钮移动时进行放大和缩小
变量收集：
    雪花池数量： 100
    向屏幕投送雪花间隔: 1000
    每次投送雪花数量：0-3
    雪花飘落全程时间：10秒

*****************************************************/
//#include <Windows.h>
//int WINAPI WinMain( __in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in LPSTR lpCmdLine, __in int nShowCmd )
int main(int argc, char *argv[])
{
	
	//初始化资源
    Q_INIT_RESOURCE(res);

    QApplication a(argc, argv);
    MainWindow *win = NULL;
    try
    {
        win = new MainWindow;
        win->showFullScreen();
    }
    catch(std::exception &e )
    {
        //std::cout << "异常" << e.what() << std::endl;
    }
    catch(...)
    {
        //std::cout << "未知"  << std::endl;
    }

    return a.exec();
}
