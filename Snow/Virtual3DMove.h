#ifndef __VIRTUAL3DMOVE_H__
#define __VIRTUAL3DMOVE_H__

#include "pubbase.h"
#include "widgetmove.h"
#include <QMouseEvent>

class QWidget;

//T 为基类，继承拥有一个QWidget *parent参数的构造函数
template <typename T>
class Virtual3DMove : public WidgetMove<T>
{
public:
	Virtual3DMove(QWidget *parent = 0):WidgetMove<T>(parent)
	{
	}
protected:
	void mousePressEvent(QMouseEvent *event)
	{
		//当鼠标左键按下时，记录当前 大小
		if(event->button() == Qt::LeftButton)
		{
			m_rect = WidgetMove<T>::geometry();
		}
		WidgetMove<T>::mousePressEvent(event);
	}

	//当移动按钮时更改按钮大小
	void mouseMoveEvent(QMouseEvent *event)
	{
		//变换说明： 当模拟成三维效果时，以左下角作为坐标原点，移动时，保持图形向四周缩放
		//支持窗体移动
		QRect newRect(m_rect);
		if (event->buttons() & Qt::LeftButton)
		{
			QPoint pos(event->globalPos() - m_CurrentPos);			
			//获取父窗体的高度
			double parentHeight = ((T*)parent())->geometry().height();
			//加10  防止分母为零
			double ratio = (parentHeight + 10 - m_rect.y() )/ (parentHeight + 10 - pos.y());
			QSize sz(m_rect.width()*ratio, m_rect.height()*ratio);
			
			//微调按钮位置
			QPoint newPos(pos.x() + (m_rect.width() - sz.width())/2, pos.y() + (m_rect.height() - sz.height())/2);
			//位置z轴越界，y小于零时，强制置为零，并加速缩小效果
			if (newPos.y() < 0)
			{
				double rat = -1.0 *  newPos.y() / m_rect.height() ;
				sz.setWidth(sz.width() *(1 - rat));
				sz.setHeight(sz.height() *(1 - rat));
				newPos.setY(0);
			}	
			newRect = QRect(newPos, sz);
		}
		//先进行move操作，防止move操作中覆盖新计算的位置和大小
		WidgetMove<T>::mouseMoveEvent(event);
		WidgetMove<T>::setGeometry(newRect);
	}
private:
	QRect m_rect;
};

#endif //__VIRTUAL3DMOVE_H__