#include "mainwindow.h"
#include <QKeyEvent>
#include <QPixmap>
#include <QBitmap>
#include <QTime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),m_snowFrame(this),m_groundLife(new GroundLife(this))
{
    //让程序无边框
    setWindowFlags( Qt::FramelessWindowHint );
    //让程序背景透明
    setAttribute(Qt::WA_TranslucentBackground, true);

    InitEnv();
}

//初始化资源
void MainWindow::InitEnv()
{
    //初始化随机数种子
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
}

MainWindow::~MainWindow()
{
	
}

void MainWindow::keyPressEvent( QKeyEvent * event )
{
    //按下esc键时，关闭
    if(event->key() == Qt::Key_Escape)
    {
        close();
    }
}

//窗口关闭时清理一下资源，恢复桌面背景
void MainWindow::closeEvent(QCloseEvent *event)
{
	if (m_groundLife)
	{
		delete m_groundLife;
	}
}